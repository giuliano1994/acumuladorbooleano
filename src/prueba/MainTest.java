package prueba;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

class MainTest {

	@Test
	void stringTestValido() {
		
		List<String> palabras = new ArrayList<>();
		
		palabras.add("hola");
		palabras.add("como");
		palabras.add("estas");
		palabras.add("soy");
		palabras.add("Giuliano");
		
		boolean resultado = Main.acumulador(palabras, "Giuliano");
		
		assertTrue(resultado);
		
	}
	
	@Test
	void stringTestNOValido() {
		
		List<String> palabras = new ArrayList<>();
		
		palabras.add("hola");
		palabras.add("como");
		palabras.add("estas");
		palabras.add("soy");
		palabras.add("Giuliano");
		
		boolean resultado = Main.acumulador(palabras, "Tomas");
		
		assertFalse(resultado);
		
	}
	
	@Test
	void integerTestValido() {
		
		List<Integer> palabras2 = new ArrayList<>();
		
		palabras2.add(1);
		palabras2.add(2);
		palabras2.add(3);
		palabras2.add(4);
		palabras2.add(5);
		
		boolean resultado = Main.acumulador(palabras2, 5);
		
		assertTrue(resultado);
		
	}
	
	@Test
	void integerTestNOValido() {
		
		List<Integer> palabras2 = new ArrayList<>();
		
		palabras2.add(1);
		palabras2.add(2);
		palabras2.add(3);
		palabras2.add(4);
		palabras2.add(5);
		
		boolean resultado = Main.acumulador(palabras2, 6);
		
		assertFalse(resultado);
		
	}

}
