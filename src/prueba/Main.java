package prueba;

import java.util.ArrayList;
import java.util.List;

public class Main {
	
	
	public static  <T> boolean acumulador(List<T> lista, T aComparar) {
		
		boolean acum = false;
		
		for (T objeto : lista) {
			
			acum = acum || comparador(objeto, aComparar);
		}
		
		return acum;
	}

	
	private static  <T> boolean comparador(T objeto, T aComparar) {
		
		return objeto.equals(aComparar);
	}

	 
	
	
	public static void main(String[] args) {
		
		List<String> palabras = new ArrayList<>();
		
		palabras.add("hola");
		palabras.add("como");
		palabras.add("estas");
		palabras.add("soy");
		palabras.add("Giuliano");
		
		boolean result = acumulador(palabras, "Giuliano");
		boolean result2 = acumulador(palabras, "Tomas");
		
		System.out.println(result);
		System.out.println(result2);
		
		
		List<Integer> palabras2 = new ArrayList<>();
		
		palabras2.add(1);
		palabras2.add(2);
		palabras2.add(3);
		palabras2.add(4);
		palabras2.add(5);
		
		boolean result3 = acumulador(palabras2, 1);
		boolean result4 = acumulador(palabras2, 6);
		
		
		System.out.println(result3);
		System.out.println(result4);
		
		
	}

}
